-- INSERTION dans la table HOTELS
INSERT INTO HOTELS VALUES(1,'HCVL-OrleansCentre','ORLEANS','15 rue du général',0689129676,'47.902964,1.909251',150,'3 etoiles');
INSERT INTO HOTELS VALUES(2,'HCVL-Tour','TOURS','15 rue du général',0689129677,'47.394144,0.68484',350,'4 etoiles');
INSERT INTO HOTELS VALUES(3,'HCVL-BOURGE','BOURGE','23 rue alambic',0689129678,'47.083328,2.4',75,'5 etoiles');
-- erreur à la prochaine ligne car clé primaire en double --
--INSERT INTO HOTELS VALUES(3,'HCVL-BOURGE','BOURGE','23 rue alambic',0689129678,'47.083328,2.4',75,'5 etoiles');

-- INSERTION dans la table CHAMBRE
INSERT INTO CHAMBRE VALUES(1,12);
INSERT INTO CHAMBRE VALUES(2,55);
INSERT INTO CHAMBRE VALUES(3,12);
-- erreur à la prochaine ligne car clé primaire en double --
--INSERT INTO CHAMBRE VALUES(3,12);

-- INSERTION dans la table DEPARTEMENT
INSERT INTO DEPARTEMENT VALUES(1,'CUISINE','MIKAL jhon', 501, 25);
INSERT INTO DEPARTEMENT VALUES(2,'CUISINE','HARISON smite', 1520, 30);
INSERT INTO DEPARTEMENT VALUES(3,'CUISINE','MIKAL ilianne', 125, 35);

-- INSERTION dans la table TYPE_CHAMBRE
INSERT INTO TYPE_CHAMBRE VALUES(1,12,1,50,4);
INSERT INTO TYPE_CHAMBRE VALUES(2,55,2,15,2);
INSERT INTO TYPE_CHAMBRE VALUES(3,12,3,18,4);

-- INSERTION dans la table SERVICES
INSERT INTO SERVICES VALUES(1,25,'livraison de repas dans la chambre','livraison de repas dans la chambre');
INSERT INTO SERVICES VALUES(2,12,'repas restaurant normal','repas pour une chambre normal');
INSERT INTO SERVICES VALUES(2,0,'repas restaurant ALL INCLUSE','repas pour une chambre ALL INCLUSE');

-- INSERTION dans la table COMMANDE
INSERT INTO COMMANDE VALUES(1,12,TO_DATE('10/05/2020','dd/mm/yyyy'),2,0,'Y',3);
INSERT INTO COMMANDE VALUES(1,12,TO_DATE('10/05/2020','dd/mm/yyyy'),1,25,'Y',3);
INSERT INTO COMMANDE VALUES(3,12,TO_DATE('15/05/2020','dd/mm/yyyy'),2,12,'N',4);

-- INSERTION dans la table PERSONNE
INSERT INTO PERSONNE VALUES(1,'MIKAL jhon','13 rue du Général Orleans',TO_DATE('01/01/1970','dd/mm/yyyy'));
INSERT INTO PERSONNE VALUES(2,'HARISON smite','29 rue du turbo Tours',TO_DATE('08/08/1981','dd/mm/yyyy'));
INSERT INTO PERSONNE VALUES(3,'MIKAL ilianne','13 rue du Général Orleans',TO_DATE('05/05/1975','dd/mm/yyyy'));
INSERT INTO PERSONNE VALUES(4,'HERALD luther','25 avenue de l_etoile',TO_DATE('01/01/65','dd/mm/yyyy'));
INSERT INTO PERSONNE VALUES(5,'DIAMOND kiale','33 road street',TO_DATE('24/11/1995','dd/mm/yyyy'));

-- INSERTION dans la table EMPLOYE
INSERT INTO EMPLOYE VALUES(1,'chef departement cuisine',1,'CUISINE');
INSERT INTO EMPLOYE VALUES(2,'chef departement cuisine',2,'CUISINE');
INSERT INTO EMPLOYE VALUES(3,'chef departement hotels',3,'CUISINE');

--INSERTION dans la table CLIENT
INSERT INTO CLIENT VALUES(1,'Français',1);
INSERT INTO CLIENT VALUES(4,'Français',0);
INSERT INTO CLIENT VALUES(5,'Anglais',3);

--INSERTION dans la table RESERVE
INSERT INTO RESERVE VALUES(1,1,12,TO_DATE('1/05/2020','dd/mm/yyyy'),TO_DATE('11/05/2020','dd/mm/yyyy'),900,0,2);
INSERT INTO RESERVE VALUES(4,2,55,TO_DATE('12/05/2020','dd/mm/yyyy'),TO_DATE('23/05/2020','dd/mm/yyyy'),1240,0,1);
INSERT INTO RESERVE VALUES(5,3,12,TO_DATE('10/05/2020','dd/mm/yyyy'),TO_DATE('11/05/2020','dd/mm/yyyy'),122,2,2);