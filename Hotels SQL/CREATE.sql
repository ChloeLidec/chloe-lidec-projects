DROP TABLE EMPLOYE;
DROP TABLE RESERVE;
DROP TABLE CLIENT;
DROP TABLE PERSONNE;
DROP TABLE TYPE_CHAMBRE;
DROP TABLE COMMANDE;
DROP TABLE CHAMBRE;
DROP TABLE DEPARTEMENT;
DROP TABLE HOTELS;
DROP TABLE SERVICES;

CREATE TABLE HOTELS(IDH number(10), nom varchar(100),
ville varchar(100),  rue varchar(50), numero number(20),
GPS varchar(100), nbre_chambre number(10), qualite varchar(100),
PRIMARY KEY(IDH));

CREATE TABLE CHAMBRE(IHC number(10), NDC varchar(50),
CONSTRAINT IHC foreign key (IHC) references HOTELS(IDH),
PRIMARY KEY(IHC, NDC));

CREATE TABLE DEPARTEMENT(IHD number(10), nomd varchar(50),
nom_resp varchar(100), superficied number(35), poste_tel number(10),
CONSTRAINT IHD foreign key (IHD) references HOTELS(IDH),
PRIMARY KEY(Ihd, nomd));

CREATE TABLE TYPE_CHAMBRE(IHT number(10), NCT varchar(50),
IDT number(25), superficiet number(35), capacite number(10),
CONSTRAINT IHT foreign key (IHT,NCT) references CHAMBRE(IHC, NDC),
PRIMARY KEY(IHT, NCT, IDT));

CREATE TABLE SERVICES(IDS number(10), prix number(10),
nom varchar(50), descriptionS varchar(100),
PRIMARY KEY(IDS, prix));

CREATE TABLE COMMANDE(IHC number(10), NDC varchar(50), JOUR date, IDS number(10), prix number(10),
paye char(1), nbre number(5),
CONSTRAINT fk_IHC foreign key (IHC,NDC) references CHAMBRE(IHC, NDC),
CONSTRAINT fk_IDS foreign key (IDS, prix) references SERVICES(IDS, prix),
PRIMARY KEY(IHC, NDC, JOUR, IDS, prix));

CREATE TABLE PERSONNE(NUMID number(10) PRIMARY KEY,nom Varchar(20), adresse varchar(50), DdN DATE);

CREATE TABLE EMPLOYE(IDemp number(10) PRIMARY KEY, fonction varchar(100),
IHD number(10) NOT NULL, nom varchar(50) NOT NULL,
CONSTRAINT fk_IDemp foreign key (IDemp) references PERSONNE(NUMID),
CONSTRAINT fk_DEPAR foreign key (IHD,nom) REFERENCES DEPARTEMENT(IHD,nomd));

CREATE TABLE CLIENT(IDclients number(10) PRIMARY KEY, nationalite varchar(100), accompagnants number(5),
CONSTRAINT fk_IDpersonne foreign key (IDclients) references PERSONNE(NUMID));

CREATE TABLE RESERVE(IDclients number(10), IHC number(10), NDC varchar(50), JOUR date, DEPART date,
prix number(10), nb_enf number(5), nb_ad number(5),
CONSTRAINT fk_IHC_NDC foreign key (IHC,NDC) references CHAMBRE(IHC, NDC),
CONSTRAINT fk_IDclient foreign key (IDclients) references CLIENT(IDclients),
PRIMARY KEY(IDclients, IHC, NDC, JOUR, DEPART));